import React, { useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import { useSelector, useDispatch } from "react-redux";
import {
  CardContent,
  Button,
  Typography,
  FormControlLabel,
  Checkbox,
  Tooltip,
  IconButton,
  Fab,
} from "@material-ui/core";
import DeleteIcon from "@material-ui/icons/Delete";
import EditIcon from "@material-ui/icons/Edit";
import { removeTodo, setComplete } from "../redux/slices/todoSlice.js";

import styles from "./todoStyle.module.css";

const SimpleCard = ({ title, status, id }) => {
  const dispatch = useDispatch();

  const classes = useStyles();
  const [statusCheck, setStatusCheck] = useState({
    checkedBox: false,
  });
  const [edit, setEdita] = useState(false);
  const [statusIn, setStatus] = useState(false);
  const [open, setOpen] = useState(false);

  const handleDelete = () => {
    console.log("event", id);
    dispatch(removeTodo(id));
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleEdit = () => {
    console.log("event");
  };

  const handleChange = (event) => {
    setStatusCheck({
      ...statusCheck,
      [event.target.name]: event.target.checked,
    });
    console.log("statusCheck", statusCheck);
    dispatch(setComplete(id));
  };

  return (
    <Card className={classes.root} variant="outlined">
      <CardContent className={classes.content}>
        <FormControlLabel
          control={
            <Checkbox
              checked={statusCheck.checkedBox}
              onChange={handleChange}
              name="checkedBox"
              color="primary"
            />
          }
          label={title}
          style={{ width: "45ch" }}
        />
        <div className={styles.styleDelete}>
          {statusCheck.checkedBox ? (
            <p className={styles.styleStatus}>Success</p>
          ) : (
            <p className={styles.styleStatus}>Unsuccess</p>
          )}

          <div>
            <Tooltip
              title="Delete"
              onClick={handleDelete}
              style={{ marginRight: "5px" }}
            >
              <IconButton
                aria-label="delete"
                placement="bottom-end"
                color="primary"
              >
                <DeleteIcon />
              </IconButton>
            </Tooltip>
            <Tooltip title="Edit" onClick={handleEdit}>
              <Fab color="secondary">
                <EditIcon />
              </Fab>
            </Tooltip>
          </div>
        </div>
      </CardContent>
    </Card>
  );
};
export default SimpleCard;

const useStyles = makeStyles((theme) => ({
  root: {
    alignItems: "center",
    width: "45ch",
    marginBottom: "20px",
    height: "18ch",
    justifyContent: "space-between",
    flexWrap: "wrap",
  },
  content: {
    alignItems: "center",
    width: "45ch",
    flexWrap: "wrap",
  },
  absolute: {
    position: "absolute",
    bottom: theme.spacing(2),
    right: theme.spacing(3),
  },
}));
