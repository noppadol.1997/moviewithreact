import React, { useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { addData, setComplete, selectTodo } from "../redux/slices/todoSlice.js";
import { makeStyles } from "@material-ui/core/styles";
import { TextField, Button } from "@material-ui/core";
import styles from "./todoStyle.module.css";
import SimpleCard from "./CardList";

const TodoList = () => {
  const [todoAdd, setTodoAdd] = useState("");
  const dispatch = useDispatch();
  const list = useSelector(selectTodo);
  const [addId, setAddId] = useState(1);
  const classes = useStyles();

  console.log("list", list);
  const showTodoList = () => {
    if (list) {
      return list.map((item, index) => {
        return <SimpleCard title={item.title} key={index} id={item.id} />;
      });
    }
    return null;
  };
  console.log("list", list);
  console.log("todoAdd", list);

  return (
    <>
      <div className={styles.flexContainer}>
        <div className={styles.styleBlockAdd}>
          <TextField
            className={classes.inputText}
            id="outlined-basic"
            label="Add List"
            variant="outlined"
            value={todoAdd}
            inputProps={{ maxLength: 30 }}
            type="text"
            onChange={(text) => setTodoAdd(text.target.value)}
          />
          <Button
            className={classes.button}
            variant="contained"
            color="primary"
            onClick={() => (
              dispatch(
                addData({
                  id: `${addId}`,
                  title: todoAdd,
                })
              ),
              setTodoAdd(""),
              setAddId(addId + 1)
            )}
          >
            ADD
          </Button>
        </div>
        <>{showTodoList()}</>
      </div>
    </>
  );
};
export default TodoList;
// export default function Todo() {
//   const todos = useSelector(selectTodo);
//   const dispatch = useDispatch();
//   const [todoTitle, setTodoTitle] = useState("");
//   console.log("todo", todos);
//   return (
//     <>
//       {todos.map((todo, idx) => {
//         <div key={idx}>
//           <p>{todo.title}</p>
//           <button onClick={() => dispatch(setComplete(idx))}>Done</button>
//         </div>;
//       })}
//       <div>
//         <input
//           type="text"
//           value={todoTitle}
//           onChange={(e) => setTodoTitle(e.target.value)}
//         />
//         <button
//           onClick={() =>
//             dispatch(
//               addTodo({
//                 title: todoTitle,
//                 complete: false,
//               })
//             )
//           }
//         >
//           Add
//         </button>
//       </div>
//     </>
//   );
// }
const useStyles = makeStyles((theme) => ({
  inputText: {
    width: "35ch",
    height: "6ch",
    // paddingLeft: "15px",
  },
  button: {
    width: "15ch",
    height: "7ch",
    marginLeft: "15px",
  },
}));
