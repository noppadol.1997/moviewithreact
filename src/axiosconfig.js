import axios from "axios";

const API_URL =
  "https://api.themoviedb.org/3/search/movie?api_key=32fe8e49e8990f25b341da765bb02771&query=a";

axios.defaults.baseURL = API_URL;

export default axios;
