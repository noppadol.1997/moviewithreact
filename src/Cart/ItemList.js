import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import {
  CardContent,
  CardMedia,
  Typography,
  IconButton,
  Grid,
  Card,
} from "@material-ui/core";
import Delete from "@material-ui/icons/Delete";
import { removeMovie } from "../redux/slices/CartSlice.js";
import { useDispatch } from "react-redux";

const OrderMovie = ({ image, title, id }) => {
  const classes = useStyles();
  console.log("image", image);
  const dispatch = useDispatch();

  const handleDelete = () => {
    dispatch(removeMovie(id));
  };

  return (
    <Card className={classes.list}>
      <CardMedia
        image={`https://image.tmdb.org/t/p/w500${image}`}
        className={classes.images}
        title={title}
      />
      <CardContent className={classes.detail}>
        <Typography variant="h5" component="h2">
          {title}
        </Typography>
        <Grid container alignItems="center" justify="space-between">
          <Grid item>
            <p>price</p>
          </Grid>
          <Grid item>
            <IconButton size="small" onClick={handleDelete}>
              <Delete></Delete>
            </IconButton>
          </Grid>
        </Grid>
      </CardContent>
    </Card>
  );
};
export default OrderMovie;

const useStyles = makeStyles((theme) => ({
  images: {
    width: "170px",
  },
  list: {
    display: "flex",
    height: "200px",
  },
  detail: {
    flex: 1,
    height: "100",
  },
}));
