import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import OrderMovie from "../Cart/ItemList";
import { makeStyles } from "@material-ui/core/styles";
import { selectMovie } from "../redux/slices/CartSlice.js";
import { resetForBuy } from "../redux/slices/CartSlice.js";
import { useDispatch } from "react-redux";
import {
  Button,
  Grid,
  Dialog,
  DialogTitle,
  DialogContentText,
  DialogContent,
  DialogActions,
} from "@material-ui/core";

const ShowListCart = () => {
  const classes = useStyles();
  const movieList = useSelector(selectMovie);
  const [open, setOpen] = useState(false);
  const [condiOfDiscout, setCondiOfDiscout] = useState("");
  const dishpatch = useDispatch();
  const handleClose = () => {
    setOpen(false);
  };

  const handleBuy = () => {
    const total = movieList.length;
    if (total >= 3 && total < 5) {
      let text = "ํYou get discount 10% ";
      setCondiOfDiscout(text);
    } else if (total >= 5) {
      let text = "ํYou get discount 20% ";
      setCondiOfDiscout(text);
    } else if (total > 0) {
      let text = "ํYou cann'tget discount  ";
      setCondiOfDiscout(text);
    } else {
      let text = "ํPlease select movie to buy";
      setCondiOfDiscout(text);
    }
    setOpen(true);
  };
  const handleForPayment = () => {
    dishpatch(resetForBuy([]));
    setOpen(false);
  };

  if (movieList) {
    console.log("movieList", movieList);
    return (
      <>
        <Grid container className={classes.gridStyle}>
          <Grid item xs={12} lg={8}>
            {movieList.map((item, index) => (
              <div className={classes.product}>
                <OrderMovie
                  image={item.image}
                  key={index}
                  id={item.id}
                  title={item.title}
                />
              </div>
            ))}
          </Grid>
          <Grid container justify="space-between" alignItems="center">
            <Grid item>
              <Button
                variant="contained"
                color="primary"
                className={classes.product}
                style={{ marginBottom: "60" }}
                onClick={handleBuy}
              >
                Buy
              </Button>
            </Grid>
            <Grid item>
              <h2>total price</h2>
            </Grid>
          </Grid>
        </Grid>
        <Dialog
          open={open}
          onClose={handleClose}
          aria-labelledby="alert-dialog-title"
          aria-describedby="alert-dialog-description"
        >
          <DialogTitle id="alert-dialog-title">
            {"Do you want to buy ?"}
          </DialogTitle>
          <DialogContent>
            <DialogContentText id="alert-dialog-description">
              {condiOfDiscout}
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button onClick={handleForPayment} color="primary">
              Yes
            </Button>
            <Button onClick={handleClose} color="primary" autoFocus>
              No
            </Button>
          </DialogActions>
        </Dialog>
      </>
    );
  }

  return null;
};
export default ShowListCart;

const useStyles = makeStyles((theme) => ({
  product: {
    marginTop: theme.spacing(2),
  },
  price: {
    marginTop: theme.spacing(2),
    textAlign: "right",
  },
  detail: {
    display: "flex",
  },
  gridStyle: {
    display: "flex",
  },
}));
