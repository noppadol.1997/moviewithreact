import React from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import { Card, Button } from "react-bootstrap";
import { useHistory } from "react-router-dom";
import { useDispatch } from "react-redux";
import { movieForBuy } from "../redux/slices/CartSlice.js";

const CardForList = ({ data }) => {
  const history = useHistory();
  const dispatch = useDispatch();

  const handleToBuy = () => {
    return dispatch(
      movieForBuy({
        id: data.id,
        title: `${data.title}`,
        image: `${data.poster_path}`,
      })
    );
  };
  const navigateToDetail = () => {
    history.push(`/Movie/${data.id}`, { params: data });
  };
  if (data) {
    return (
      <Card
        style={{
          width: "260px",
          height: "30rem",
        }}
      >
        <Card.Img
          variant="top"
          src={`https://image.tmdb.org/t/p/w500${data.poster_path}`}
          style={{
            width: "230px",
            height: "300px",
            alignSelf: "center",
            marginTop: "10px",
          }}
        />
        <Card.Body>
          <Card.Title>{data.title}</Card.Title>
          <Card.Text>Rating {data.vote_average}</Card.Text>
          <div style={{ display: "flex" }}>
            <Button onClick={navigateToDetail} variant="primary">
              Detail
            </Button>
            <Button onClick={handleToBuy} variant="primary">
              Buy
            </Button>
          </div>
        </Card.Body>
      </Card>
    );
  }
  return null;
};

export default CardForList;
