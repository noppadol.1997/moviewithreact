import Home from "../src/Home/Home";
import ListOfMovie from "../src/Movie/ListOfMovie";
import Todo from "../src/todo/AddTodo";
import { BrowserRouter as Router, Route } from "react-router-dom";
import ShowListCart from "../src/Cart/ShowList";
import MovieDetail from "../src/Movie/MovieDetail";

function App() {
  return (
    <Router>
      <Home />
      <Route exact path="/Cart">
        <ShowListCart />
      </Route>
      <Route exact path="/">
        <ListOfMovie />
      </Route>
      <Route exact path="/todo">
        <Todo />
      </Route>
      <Route exact path="/Movie/:id">
        <MovieDetail />
      </Route>
    </Router>
  );
}

export default App;
