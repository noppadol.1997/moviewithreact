import React, { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { useLocation } from "react-router-dom";
import { makeStyles } from "@material-ui/core/styles";
import { movieForBuy } from "../redux/slices/CartSlice.js";

import {
  CardContent,
  Button,
  Typography,
  Grid,
  Card,
  CardMedia,
  CardActions,
} from "@material-ui/core";

const MovieDetail = () => {
  const classes = useStyles();
  const [movieDetail, setMovieDetail] = useState({});
  const location = useLocation();
  const data = location.state.params;
  const dispatch = useDispatch();

  const handleToBuy = () => {
    return dispatch(
      movieForBuy({
        id: data.id,
        title: `${data.title}`,
        image: `${data.poster_path}`,
      })
    );
  };
  if (!data) return null;

  return (
    <Grid>
      <Card className={classes.root}>
        <CardMedia
          className={classes.media}
          image={`https://image.tmdb.org/t/p/w500${data.poster_path}`}
          title={data.title}
        />
        <CardContent>
          <Typography gutterBottom variant="h5" component="h2">
            {data.title}
          </Typography>
          <Typography variant="body2" color="textSecondary" component="p">
            {data.overview}
          </Typography>
        </CardContent>
        <CardActions>
          <Button size="small" color="primary" onClick={handleToBuy}>
            Buy
          </Button>
        </CardActions>
      </Card>
    </Grid>
  );
};
export default MovieDetail;
const useStyles = makeStyles({
  root: {
    maxWidth: 380,
    marginTop: 60,
    marginLeft: 20,
    marginRight: 20,
    marginBottom: 20,
  },
  media: {
    width: "auto",
    height: "400px",
    paddingTop: 20,
  },
});
