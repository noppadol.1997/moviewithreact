import React, { useEffect, useState } from "react";
import axios from "axios";
import CardForList from "../component/Card";
import { Container, Row, Col } from "react-bootstrap";
import styles from "./InfoMovie.module.css";

const InfoMovieContainer = () => {
  const [responseData, setResponseData] = useState([]);

  useEffect(() => {
    fetchMovieInfo();
  }, []);

  const fetchMovieInfo = async () => {
    const res = await axios.get(
      "https://api.themoviedb.org/3/search/movie?api_key=32fe8e49e8990f25b341da765bb02771&query=a"
    );
    let dataRes = res.data.results;
    console.log("response:", dataRes);
    setResponseData(dataRes);
  };
  return (
    <Container>
      <Row>
        {responseData.map((item) => {
          return (
            <Col sm={6} lg={4} xs={12} className={styles.test}>
              <CardForList data={item} key={item.id} />
            </Col>
          );
        })}
      </Row>
    </Container>
  );
};
export default InfoMovieContainer;
