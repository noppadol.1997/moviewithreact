import { combineReducers } from "@reduxjs/toolkit";
import todoListReducer from "./slices/todoSlice";
import cartReducer from "./slices/CartSlice";

const rootReducer = combineReducers({
  todoListSlice: todoListReducer,
  cartSlice: cartReducer,
});

export default rootReducer;
