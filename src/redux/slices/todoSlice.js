import { createSlice } from "@reduxjs/toolkit";

const todoList = createSlice({
  name: "todoLists",
  initialState: { data: [] },
  reducers: {
    addData: (state, action) => {
      console.log("action", action.payload);
      state.data.push(action.payload);
    },

    setComplete: (state, action) => {
      const todo = state.data.find((todo) => todo.id === action.payload);
      if (todo) {
        todo.completed = !todo.completed;
      }
    },
    removeTodo: (state, action) => {
      return { data: state.data.filter((data) => data.id != action.payload) };
    },
  },
  extraReducers: {},
});
export const selectTodo = (state) => state.todoListSlice.data;
export const { addData, setComplete, removeTodo } = todoList.actions;
export default todoList.reducer;
