import { createSlice } from "@reduxjs/toolkit";

const cart = createSlice({
  name: "cartSlice",
  initialState: { data: [] },
  reducers: {
    movieForBuy: (state, action) => {
      console.log("action", action.payload);
      state.data.push(action.payload);
    },

    removeMovie: (state, action) => {
      return { data: state.data.filter((data) => data.id != action.payload) };
    },
    resetForBuy: (state, action) => {
      return { data: action.payload };
    },
  },
  extraReducers: {},
});
export const selectMovie = (state) => state.cartSlice.data;
export const { movieForBuy, removeMovie, resetForBuy } = cart.actions;
export default cart.reducer;
