import { configureStore, getDefaultMiddleware } from "@reduxjs/toolkit";
import rootReducer from "./rootReducer";

const middlewareTest = [];
const store = configureStore({
  reducer: rootReducer,
  middleware: getDefaultMiddleware().concat(middlewareTest),
});

export default store;
